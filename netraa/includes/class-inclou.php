<?php
/**
 * Netraa Inclou.
 *
 * @since   0.0.0
 * @package Netraa
 */

/**
 * Netraa Inclou.
 *
 * @since 0.0.0
 */
class N_Inclou {
	/**
	 * Parent plugin class.
	 *
	 * @since 0.0.0
	 *
	 * @var   Netraa
	 */
	protected $plugin = null;

	/**
	 * Constructor.
	 *
	 * @since  0.0.0
	 *
	 * @param  Netraa $plugin Main plugin object.
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {

	}
}
